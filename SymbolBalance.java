import java.io.*;

public class SymbolBalance {
	
	//function that converts text file to String
	public static String getText( String fileName ) {
		
		String text = "";
		
		try {
			
			//open file
			BufferedReader br =new BufferedReader(new FileReader(fileName));
			
			//declare variables
			String line;
			
			while((line = br.readLine()) !=null) {
				text = text + line + "\n";
			}
			br.close();
			
		} catch (IOException e) {
			System.out.println(e);
		}
		
		return text;
		
	}
	
	public static final void main(String[] args) {
		
		String fileName = args[0];
				
		String text = getText(fileName);
		
		
		//create stack
		MyStack<Character> stack = new MyStack<Character>();
		
		//scan String char by char, if char, push it,
		String open = "{[(";
		String close = ")]}\"";
		String comment = "/*";
		
		//how to identify comment:
		//when the top of stack is /*, comment. When encounter */, exit comment
		boolean isComment = false;
		for (int i = 0; i < text.length(); i++) {
			char ch = text.charAt(i);
			
			if (!isComment && open.indexOf(ch) >= 0) {
				stack.push(ch);
			}
			else if (!isComment && close.indexOf(ch) >= 0) {
				
				//peek before push closing to get the opening symbol
				char temp; 
				if (stack.peek() == null) temp = '\u0000'; //default char, from stackOverflow, https://stackoverflow.com/questions/9909333/whats-the-default-value-of-char
				else temp = stack.peek();
				
				stack.push(ch);
				
				if (ch == ')' && temp == '(') {
					stack.pop();
					stack.pop();
				}
				else if (ch == ']' && temp == '[') {
					stack.pop();
					stack.pop();
				}
				else if (ch == '}' && temp == '{') {
					stack.pop();
					stack.pop();
				}
				else if (ch == '\"') {
					
					if (temp != ch) {
					} else {
						stack.pop();
						stack.pop();
					}
				}
				else {
					if (temp != '\u0000' && close.indexOf(temp) < 0) {
						System.out.println("Unbalanced! Symbol "+ temp + " and " + ch +" are mismatched. ");
						return;
					}
				}
			}
			else if (comment.indexOf(ch) >= 0) {
				
				//if found a /, scan again
				if (ch == '/' && !isComment) {
					//scan again
					stack.push(ch);
					i++;
					if (text.charAt(i) == '*') {
						stack.push('*');
						isComment = true;
					}
				}
				else if (ch == '*' && isComment) {
					i++;
					if (text.charAt(i) == '/') {
						stack.pop();
						stack.pop();
						isComment = false;
					}
				}
			}
		}
		
		//when passed mismatch test, check size of stack
		while (stack.peek() == '/' || stack.peek() == '*')
			stack.pop();
		
		if (stack.size > 0) {
			if (open.indexOf(stack.peek()) >= 0) {
				System.out.println(stack.size + " opening symbols do not have closing symbols! The top symbol is "+stack.peek());
				return;
			}
			if (close.indexOf(stack.peek()) >= 0) {
				System.out.println(stack.size + " closing symbols do not have opening symbols! The top symbol is "+stack.peek());
				return;
			}
			
		} else
			System.out.println("All symbols are balanced!");
		
	}
}
