import java.io.*;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;

public class SpellChecker {

	public static final void main(String args[]) {

		//read command line argumet
		String dictName;
		String textName;
		HashMap<String,String> dict = new HashMap<>();
		HashMap<String,String> text = new HashMap<>();
		HashSet<String> misspell = new HashSet<>();

		try {
			dictName = args[0];
			textName = args[1];
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Command Line Argument Missing");
			return;
		}

		dict = wordsToMap(dictName);
		text = wordsToMap(textName);
		//compile misspell list
		misspell = checkMisspell(dict, text);



		//print out misspell
		System.out.println("List of misspell words:");
		for (String word : misspell) {
			String[] suggestWords = suggestion(dict, word);
			System.out.println(word + " | line: " + text.get(word));
			if (suggestWords[0].length()+suggestWords[1].length()+suggestWords[2].length() > 0) {
				System.out.println("Suggested correction:");
				if (suggestWords[0].length()>0)
					System.out.println("Add one character: "+suggestWords[0]);
				if (suggestWords[1].length()>0)
					System.out.println("Remove one character: "+suggestWords[1]);
				if (suggestWords[2].length()>0)
					System.out.println("Exchange adjacent characters: "+suggestWords[2]);
			} else {
				System.out.println("No suggestions available.");
			}
			System.out.println();
		}

	}

	public static String[] suggestion(HashMap<String,String> dict, String word) {
		return new String[] {ruleOne(dict, word), ruleTwo(dict, word), ruleThree(dict, word)};
	}

	public static String ruleOne(HashMap<String,String> dict, String word) {
		//adding one character is the same thing as:
		//1. get all words that has word length + 1
		//2. check that every char is present in the potential word
		//3. if the above condition passed, add the word to the String
		String wordList = "";
		for (String aWord : dict.keySet()) {
			if ((aWord.length() - word.length()) == 1) {
				boolean valid = true; 
				//loop through each character of word
				for (int i = 0; i < word.length() ; i++) {
					//compare char from both arrays at the same time, when encounter a mismatch, 
					//take the word out of aWord, then compare again, if match, then it is a valid word
					if (word.charAt(i) != aWord.charAt(i)) {
						//https://stackoverflow.com/questions/20304671/delete-char-at-position-in-string
						StringBuilder sb = new StringBuilder(aWord);
						String temp = sb.deleteCharAt(i).toString();
						if (!temp.equals(word)) {
							valid = false;
						}
						break;
					}
				}
				if (valid)
					wordList += (aWord + " | ");
			}
		}
		return wordList;
	}

	public static String ruleTwo(HashMap<String,String> dict, String word) {
		//basically the same as the algorithm above
		String wordList = "";
		for (String aWord : dict.keySet()) {
			if ((word.length() - aWord.length()) == 1) {
				boolean valid = true; 
				//loop through each character of word
				for (int i = 0; i < aWord.length() ; i++) {
					//compare char from both arrays at the same time, when encounter a mismatch, 
					//take the word out of aWord, then compare again, if match, then it is a valid word
					if (word.charAt(i) != aWord.charAt(i)) {
						//https://stackoverflow.com/questions/20304671/delete-char-at-position-in-string
						StringBuilder sb = new StringBuilder(word);
						String temp = sb.deleteCharAt(i).toString();
						if (!temp.equals(aWord)) {
							valid = false;
						}
						break;
					}
				}
				if (valid)
					wordList += (aWord + " | ");
			}
		}
		return wordList;
	}

	public static String ruleThree(HashMap<String,String> dict, String word) {
		String wordList = "";
		ArrayList<String> swappedList = new ArrayList<>();

		//base case, if only has one letter, no suggestion available 
		if (word.length() < 2)
			return wordList;
		else {
			//compile a list of swapped words
			for (int i = 1; i < word.length(); i++) {
				String swapped = word.substring(0, i-1) + word.charAt(i) + word.charAt(i-1) + word.substring(i+1);
				// System.out.println("Swapped word: "+swapped);
				swappedList.add(swapped);
			}
		}

		//check swapped words against dict
		for (String aWord : dict.keySet()) {
			for (String temp : swappedList)
				if (temp.equals(aWord))
					wordList += (aWord+" | ");
		}

		return wordList;
	}

	public static HashSet<String> checkMisspell(HashMap<String,String> dict, HashMap<String,String> text) {
		HashSet<String> misspell = new HashSet<>();
		for (String word : text.keySet()) {
			if (!dict.containsKey(word))
				misspell.add(word);
		}
		return misspell;
	}

	public static HashMap<String,String> wordsToMap(String fileName) {
		HashMap<String,String> map = new HashMap<>();
		try {
			//open file
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			String[] words;
			int count = 1;

			//make a hask map
			while ((line = br.readLine()) != null) {
				//in case dictionary is not formatted correctly
				words = parseWords(line);
				for (String word : words) {
					//if the entry already exists, update value with another line number
					if (map.containsKey(word)) {
						map.put(word, map.get(word)+" "+count);
					} else {
						map.put(word, ""+count); 
					}
				}

				count++;
			}

		} catch (IOException e) {
			System.out.println(e);
		}

		return map;
	}
	
	public static String[] parseWords(String line) {
		String[] words = line.split("\\s+");
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replaceAll("^[^a-zA-Z0-9]+", "").replaceAll("[^a-zA-Z0-9]+$", "").toLowerCase();
		}
		return words;
	}
	
}